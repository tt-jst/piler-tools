#!/usr/bin/perl -w

#####################################################################
### Copyright 2021 Jörn Stein <j.stein@textechno.com>
###
### Permission is hereby granted, free of charge, to any person 
### obtaining a copy of this software and associated documentation 
### files (the "Software"), to deal in the Software without
###  restriction, including without limitation the rights to use,
### copy, modify, merge, publish, distribute, sublicense, and/or
### sell copies of the Software, and to permit persons to whom the
### Software is furnished to do so, subject to the following
### conditions:
###
### The above copyright notice and this permission notice shall be
### included in all copies or substantial portions of the Software.
### 
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
### FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
### OTHER DEALINGS IN THE SOFTWARE.
#####################################################################

# Usage:
# - Copy this file together with PilerTools.pm and PilerTools.conf
#   into the same directory. 
# - Create a database user with readonly access to the piler database
# - Adjust the database credentials in PilerTools.conf
# - Adjust the paths to write to below

use File::Copy;
use File::Basename;
use File::Path qw/make_path/;
use Getopt::Long;
use FileHandle;
use POSIX qw(strftime);
use DBI;
use Time::Piece;

# Start with a random version number...
my $version = "0.3.18";

chdir "/tmp";

# Verbosity level
# 0 - onlystart/stop and errors (identifying the mail in question)
# 1 - written file name and major date errors
# 2 - minor date issues
# 3 - other
my $verbose = 0;

# Get path to current script
my $script_dir=dirname(__FILE__);

# The path were the data for this script is stored
my $base_path="/path/to/piler-export";

# File with PID for running process. Might be necessary to delete manually
my $pidfile="/var/run/com.company.piler-export.pid";

# Base directory where the exported files should be stored
my $export_dir = "$base_path/by-date";

# Database credentials. Override with --db_user=... and --dbpassword=...
my $db_dsn="DBI:mysql:piler";
my $db_user = "pilerro";
my $db_password = "piler-read";

# File where details are written if some problems occur during export
my $error_log;

# Filename for the non-error log messages
my $export_log;

# Generic file handle variable
my $fh;

# Flag to indicate that the processing loop should exit due to CTRL-C
my $break = 0;

###
### Parameters
### 

# Index of last message
my $last_index = -1;

# Overwrite existing files
my $force_overwrite=0;

# The index of the first message to be exported
my $manual_index = 0;

# Maximum number of rows to retrieve in one run
my $max_rows = 0;

# The piler_id of the message to be exported
my $piler_id = "";

Getopt::Long::Configure ("bundling");
my $options_ok = GetOptions(
	# If set, will overwrite existing messages
	'f'          => \$force_overwrite,

	# Set starting index (instead of remembering from a previous run)
  	'i=i'        => \$manual_index,

	# Number of mails to export
	'n=i'        => \$max_rows,

	# Extracts a single mail with the given piler_id
	'piler_id=s' => \$piler_id,

	# Increase verbosity level
	'v+'         => \$verbose,

	# Path where files are exported
	'export_dir=s' => \$export_dir,

	# Location of the PID file used to detect previous processes
	'pid=s' => \$pidfile,

	# Location of the error log
	'error_log=s' => \$error_log,

	# Location of the error log
	'export_log=s' => \$export_log,

	# Database credentials
	'db_user=s' => \$db_user,
	'db_password=s' => \$db_password,
	'db_dsn=s' => \$db_dsn,

);

unless ($options_ok) {
	usage(1);
}

usage(1) unless ($max_rows > 0 || $piler_id ne "");


if (-f $pidfile) {

	open $fh, "<", $pidfile;
	my $oldpid = <$fh> ;
	close $fh;

	$oldpid =~ s/^([0-9]+) .*/$1/;

	if ($oldpid > 0) {
		my $exists = kill 0, $oldpid;
		$exists && die "Another export-script is already running: $oldpid\n";
	}
}

# File where details are written if some problems occur during export
if (!defined($error_log)) {
	$error_log = "$export_dir/log/" . strftime("%Y-%m-%d-%H-%M-%S", localtime(time())) . "-export.err";
}
if (!defined($export_log)) {
	$export_log = "$export_dir/log/" . strftime("%Y-%m-%d-%H-%M-%S", localtime(time())) . "-export.log";
}

# Create the path for error logging
-d dirname($error_log) || make_path(dirname($error_log)) || die "FAILED to create error log directory";
-d dirname($export_log) || make_path(dirname($export_log)) || die "FAILED to create export log directory";

&log("$0 v$version\n", 0);

&log("Using verbosity $verbose\n", 2);

&log("Using PID file $pidfile\n", 2);

open($fh, ">", $pidfile);
print $fh "$$ $0";
close($fh);

&log("Using export dir $export_dir\n", 2);

# The file where the index of the last retrieved message is stored
my $index_file="$export_dir/lastExport";
&log("Using index file $index_file\n", 2);

&log("Using error log $error_log\n", 2);
&log("Using export log $export_log\n", 2);


# Handle for error log
my $err_h; 
# Handle for export log
my $exp_h;

# Make sure the path for the index file exists
my $runpath=dirname($index_file);
-d $runpath || make_path $runpath;

# Open the database for reading
my $dbh = &piler_db_open();

# Requesting a specific piler_id comes first
if ($piler_id ne "") {
	$manual_index = &get_id_for_piler_id($piler_id, $dbh);
	&log("Index for $piler_id: $manual_index\n", 2);
	$max_rows = 1;
} 

# A manual index (can also be set from piler_id) also take precedence.
if ($manual_index > 0) {
	$last_index = $manual_index - 1;
}

# If no index has been set yet, try to get one from the file
# Otherwise, just start at 0
if (-f $index_file && $last_index < 0) {
	open ($fh, '<', $index_file
) or die "Failed to open message index";	
	$last_index = <$fh>;
	close $fh;
}

&log("Starting index $last_index\n",1);

# Allow program to exit gracefully upon CTRL-C
$SIG{INT} = \&break;

# Construct the query string to only get mails that entered piler more than
# 60 seconds ago. This will allow piler hopefully enough time to get all databases
# in order.
my $query_string = "SELECT id,piler_id,sent FROM metadata where id>$last_index and arrived<" . (time() - 60) . " order by id limit $max_rows;";

&log("SQL: $query_string\n", 2);

my $query = $dbh->prepare($query_string);
$query->execute();

my $processed = 0;

while (my @row = $query->fetchrow_array()){
	$processed += 1;


&log("$row[0]\t$row[1]\t$row[2]\n",3);

	# Workaround for files that were previously store in localtime
	my $target_dir_local = strftime("%Y/%m/%d", localtime($row[2]));

	# Target dir from piler timestamp
	my $target_dir = strftime("%Y/%m/%d", gmtime($row[2]));

	# Base name of EML file, with piler id and piler_id
	my $target_file = "$row[0]-$row[1].eml";

	# Get the complete email from piler
	my @mail = `/usr/local/bin/pilerget $row[1]`;

	if (scalar(@mail) < 3) {
		# Email to small - problem?
		&log("ERROR: pilerget $row[1] FAILED\n@mail\n\n", -1);
		next;
	}

	my ($date_check_result, $mailref, $mail_date) = piler_fix_date(\@mail, 0, $verbose);

	&log("Real date... " . $mail_date->strftime("%Y/%m/%d %H:%M:%S") . "\n", 2);

	# Recalculate the target path with the information from the mail
	my $target_new = $mail_date->strftime("%Y/%m/%d");
	if ($target_new ne $target_dir) {

		&log("$row[0] Date mismatch. ", 2);
		&log("Piler: " . strftime("%Y/%m/%d %H:%M:%S", gmtime($row[2])) .", ", 2);
		&log("Mail: " . $mail_date->strftime("%Y/%m/%d %H:%M:%S"), 2);

		$target_dir = $target_new;
	}

	$export_file = "$export_dir/$target_dir/" . strftime("%H%M%S", gmtime($row[2])) . "-$target_file";

	if ( ! -f $export_file || $force_overwrite) {		

		# Fix the attachments
		my ($rep, $fail, $mailref) = &piler_fix_file($dbh, $mailref, $verbose);

		$fail > 0 && &log("$row[0] ERROR fixing attachments\n", -1);
		$verbose >= 1 && $fail > 0 && print "$row[0] Fixed attachment pointers\n";


		make_path "$export_dir/$target_dir" || die "FAILED to create $export_dir/$target_dir";

		piler_purge_file($export_file, $verbose);

&log("$row[0] -> $export_file\n", 1);

		piler_write_file($export_file, $mailref, $verbose);
	 
	} else {
&log(sprintf("%8d", $row[0]) . " to $export_file... Skipping\n", 3);
	}

	$last_index = $row[0];

	$break == 1 && last;
}

# All mails processed, or interrupted.

# Close the database
$query->finish();
&piler_db_close($dbh);

&log("Exported $processed, up to $last_index\n", 1);

# Write the index back to file
if ($manual_index == 0) {
	open ($fh, '>', $index_file) or die "Failed to open message index for writing";
	print $fh $last_index;
	close $fh;
}

# Close the error log
defined($err_h) && close($err_h);

# Close the error log
defined($exp_h) && close($exp_h);


# Remove the PID file
unlink($pidfile);

sub log {
	my $msg = shift @_;
	my $level = shift @_;

	$level = 1 unless defined($level);

	if ($level < 0 && defined($error_log)) {
		# Write to error log file
		if (!defined($err_h)) {
			 open ($err_h, ">>", $error_log); 
		}
		print $err_h $msg;
	}

	#Write if verbosity level fits
	if ($level <= $verbose) {
		if (defined($export_log)) {
			if (!defined($exp_h)) {
				open ($exp_h, ">>", $export_log); 
			}
			print $exp_h $msg;
		}
		print $msg;
	}
}

# This gets called by the CTRL-C signal handler. It sets a flag to tell the main loop to stop.
sub break {
	$break = 1;
}


sub usage {

	my $exit_code = defined($_) ? $_ : 0;

	print <<"EOF";

piler-export.pl - Export emails from piler and fix attachments when needed

$0 -n numberOfMails [-f] [-i startIndex] [--piler_id pilerId] [-v] [-p]

	-f              	Force overwriting of existing messages
	-n              	Number of messages to export
	-i              	First message number to export 
	--piler_id id   	ID of a single message to export
	-v              	Increase verbosity level
    --export_dir path	Path where eml files are exported
	--pid=filename		Filename where PID file is stored
	--db_user=...		Database user
	--db_password=...	Password for database user
	--error_log=file	Logfile for error messages
	--export_log=file 	Logfile for logging

Exported mails are written into a dated subdirectory under $export_dir.
Several other information is stored in subdirectories under $base_path.
The paths can be configured in the source code.

If no piler_id or starting index is given via the --piler_id or -i switches, 
the script will start at the first message and remember the position for
future runs in $index_file. Therefore the script can be set up with the
correct paths and then be run by cron to update the exported emails.

EOF

	exit $exit_code;
}


sub read_config {
	my $file = shift @_;

	-f $file || die "Failed to read configuration from '$file'\n";

	open my $fh, "<", $file;
	my %hash = map { split /=|\s+/; } <$fh>;
	close $fh;

	return %hash;
}

# Opens the piler database
# Params:
# Returns:
# - Database Handle
sub piler_db_open() {

	return DBI->connect($db_dsn, $db_user, $db_password);

}

# Close the piler database
# Params:
# - Database handle
# Returns:
sub piler_db_close {

	my $dbh = shift @_;
	$dbh->disconnect();

}

sub piler_restore_file {
	my ($arg, $verbose) = @_;

	# The file extension for backup files before missing attachments are replaced 
	my $attext = "MissingPilerAttachment";

	# If this script was run before, there will be a copy of the file with the ".noatt" ending
	# Restore that to the original attachment_name.
	if (-f "$arg.$attext") {
&log("Restoring previous version... ", 1);
		move("$arg.$attext", $arg);
	}
}

# Read a mail from a file and store it in an array of strings. Return the reference of
# the array
# Parameters:
# - attachment_name
#
# Returns:
# array reference with file contents
sub piler_read_file {

	my ($arg, $verbose) = @_;


	&piler_restore_file($arg, $verbose);

&log("Reading $arg... ", 1);

	# Open the original file for reading
	open(MAIL, $arg) or die("Could not open $arg");

	my @mail = <MAIL>;

	close MAIL;

&log("(" . scalar(@mail) . " lines).\n", 1);

	return \@mail;
}

sub piler_backup_file {
	
	my $arg = shift @_;
	my $verbose = shift @_;

	# The file extension for backup files before missing attachments are replaced 
	my $attext = "MissingPilerAttachment";

&log("Backing up $arg\n", 2);

	-f $arg && move($arg,"$arg.$attext");

}

sub piler_write_file {
	
	my $arg = shift @_;
	my $mailref = shift @_;
	my $verbose = shift @_;

	# The file extension for backup files before missing attachments are replaced 
	my $attext = "MissingPilerAttachment";

&log("Writing file $arg... ", 2);

	move($arg,"$arg.$attext");
	open OUT, ">", $arg or die("Could not open $arg for writing");
	print OUT @$mailref;
	close OUT;

&log("Done.\n", 2);
}

# Remove all files that begin with the given string
sub piler_purge_file {

	my $arg = shift @_;
	my $verbose = shift @_;

	my @local_files = glob $arg . "*";
		foreach (@local_files) {
&log("unlink $_\n", 2);
			unlink("$_");
		}

}

# Check a file for missing attachments (denoted by a ATTACHMENT_POINTER marker) and 
# try to retrieve it from piler
# Parameters:
# - database handle
# - reference to array of strings (the file)
sub piler_fix_file {

	my $dbh=      shift @_; 
	my $mailref=  shift @_;
	my $verbose=  shift @_;

	my $from;
	my $subject;
	my $date;
	my $attachment_name;

&log("Checking attachments... ", 2);

	# Will be set to 1 if an attachment pointer is found
	my $rep = 0;

	# Flag to indicate failure (no copying, but display status string)
	my $fail = 0;

	# Store the complete mail file until we know if it should be replaced
	# my @result = ();

	my $is_first_header = 1;

	$from = "<UNSPECIFIED>" ;
	$subject = "<UNSPECIFIED>";
	$date = "<UNSPECIFIED>";
	$attachment_name = "<UNSPECIFIED>";

	my $sqlstatus = "";

	foreach(@$mailref){

#print "parsing line $_";
		# If the current line matches the missing attachment indicator, get the piler_id and attachment index,
		# then download the attachment from piler and replace the ATTACHMENT_POINTER line
		
		$from = $_ if (/^From: (.*)/i && $from eq "<UNSPECIFIED>");
		$subject = $_ if (/^Subject: (.*)/i && $subject eq "<UNSPECIFIED>");
		# $date = $_ if (/^Date: (.*)/ && $date eq "<UNSPECIFIED>");
		$attachment_name = "<UNSPECIFIED>" if /^--/;
		$attachment_name = $1 if /attachment_name=(.*?);/;

		# an Empty line ends the header
		$is_first_header = 0 if /^$/;

		if (/^ATTACHMENT_POINTER_([0-9a-f]*).a([0-9]*)_XXX_PILER/) {
			my $att;
			my $result;
			($result, $att) = &retrieve_attachment($1, $2, $dbh, $verbose);
			if ($result == 0) {
				s/ATTACHMENT_POINTER_([0-9a-f]*).a([0-9]*)_XXX_PILER/$att/;
				$rep+=1;
			} elsif ($result == 1 && s/ATTACHMENT_POINTER_[0-9a-f]*.a[0-9]*_XXX_PILER(--|$)/$1/) {
				s/ATTACHMENT_POINTER_([0-9a-f]*).a([0-9]*)_XXX_PILER--/--/;
				$rep+=1;
&log("Removing nonexistent attachment pointer.\n", 1);
			} else {
				$fail += 1;
&log("FAILED TO RETRIEVE ATTACHMENT. \n", 1);
			}
		}

	}

&log("Attachment check done.\n", 2);
	
	if ($fail > 0) {
&log("$from$date$subject\n", 2);
		return ($rep, $fail, $mailref);
	}

	return ($rep, $fail, $mailref);
	
}


# Check a mail for malformed date in the Date: field. If the Date: field
# is bad, get a correct date from the Received: headers.
# Poossibly overwrite the Date: header with the correct value, this will be UTC then.
# Parameters:
# - filename to the mail
# - reference to array of strings (the file)
# - fix date field
# - verbosity level
sub piler_fix_date {

	my $mailref=  shift @_;
	my $fix_date = shift @_;
	my $verbose = shift @_;

	my $date;

	&log("Checking date headers... \n", 2);

	my $is_first_header = 1;
	my $inside_received = 0;

	my $rewrite_recv;
	my $recv_valid = 0;
	my $received_date;
	my $resend_date;
	my $tp_recv;

	foreach(@$mailref){

		# Examine the Received: headers. Take any valid date from them. Later
		# finds replace earlier ones, so the last one should be pretty close to 
		# the actual sending time.


		# an Empty line ends the header
		$is_first_header = 0 if /^$/;

		if (/^Received:/i && $is_first_header) {
			# The Received: header begins with the word Received:.  DUH!
			$inside_received = 1;
		} elsif ($inside_received && /^\S/) {
			# The received header ends when a line begins with a non-whitespace character
			$inside_received = 0;
		}

		if (/^Resent-To:/i && $is_first_header) {
			# IF the mail was resend, store the previous received date and unset it so
			# it can be taken from after the REsend header
&log("Resend: $received_date\n", 2);			
			$resend_date = $received_date;
			undef $received_date;
		}

		# Take the first usable date from the Received headers. While the last Received header
		# is closer to the original sending date, it is unfortunately sometimes as bad as the 
		# Date header...
		if ($inside_received && !defined($received_date)) {
			# Take the last part of the ;-separated components
			my @parts = split(";");
			my $received = pop @parts;

			if ($received =~ /(\w{3}, +\d{1,2} \w{3} \d{4} \d{2}:\d{2}:\d{2} [+-]\d{4})/) {
				$received_date = $1;
				&log("Received: $received_date\n", 2);
			}

		}

		if (/^Date:\s(.*)$/i) {

&log($_, 2);

			my $maildate = $1; 
			$maildate =~ s/^\s*(.*?)\s*$/$1/;

			my ($date_valid, $rewrite_date, $tp_date) = &read_time($maildate, $verbose);
			
			# Use Resend date if no further received date was found
			if (!defined($received_date) && defined($resend_date)) {
				$received_date = $resend_date;
			}

			# Convert received date to epoch
			if (defined($received_date)) {
				($recv_valid, $rewrite_recv, $tp_recv) = &read_time($received_date, $verbose);
			}

			# tp_new will only be set if dates differ too much or date is malformed (but valid)
			my $tp_new = $tp_date;
			my $date_was_replaced = 0;

			if ($date_valid) {
				if ($recv_valid) {
					if (abs($tp_recv->epoch - $tp_date->epoch) > 86400 * 30) {
						$tp_new = $tp_recv;
						$date_was_replaced = 1;
					}
				}
			} else {
				if ($recv_valid) {
					$tp_new = $tp_recv;
					$date_was_replaced = 1;
				} else {
					# FAIL: no valid date!
					&log("FAIL: No Received: and no Date: header!\n", -1);
					return (0, $mailref, &dummy_time());
				}
			}

			if ($date_was_replaced) {

				if ($fix_date ) {

					$_ =~ s/^Date:.*$/Date: $received_date/;
#&log("Receive: $received_date\nDate:    $maildate\nRewrite: $new_date... ", 1);
&log("Rewrite data $maildate to $received_date.\n", 1);

				} else {

&log("Use $received_date instead of $maildate.\n", 2);

				}

				return (1, $mailref, $tp_recv);

			}

			
if (defined($received_date)) { &log("Recv: $received_date ($tp_recv)\n", 2); }
&log("Date: $maildate ($tp_date)\n", 2);
			
			return (0, $mailref, $tp_date);
	
		}
	}

	# No Date: and no Received: ? That's bad...
	if (!defined($received_date)) {
&log("FAIL: No Received: and no Date: header!\n", -1);
		return (0, $mailref, &dummy_time());
	}

	# No date header found. Return the Received date
	($recv_valid, $rewrite_recv, $tp_recv) = &read_time($received_date, $verbose);

	if (!$recv_valid) {
&log("FAIL: No Date: header and could not parse '$received_date'!\n", -1);

		return (0, $mailref, &dummy_time());
	}
	

&log("No Date: header. Using received date: $received_date\n", 2);
	return (0, $mailref, $tp_recv);
	
}

sub dummy_time {
	my $tm = gmtime('0');
	return $tm;
}

# Try to parse a datetime string to an epoch time. Returns 0 if none of the known formats can
# be matched.
sub read_time {
	my ($timestring, $verbose) = @_;

	my $result;
	my $err_code = 1;

	# If 1, date string is not in the correct format and should be rewritten
	my $need_rewrite = 1;

	# Date string contained a valid date, maybe in a bad format
	my $date_valid = 1;

	if (!defined($timestring)) {

		&log("Undefined datetime string.\n", -1);
		return (1, gmtime('0'));

	} 
	
	$timestring =~ s/([:.]\d{2})\s+GMT/$1 +0000/;

	if ($timestring =~ /(\w{3},\s+\d{1,2}\s+\w{3}\s+\d{4}\s+\d{2}:\d{2}:\d{2}\s+[+\-]\d{4})/) {

		($err_code, $result) = try_parse_time($1, "%a, %d %b %Y %H:%M:%S %z", $verbose);
		$need_rewrite = 0;
		$date_valid = 1;
		
	} elsif ($timestring =~ /(\d{14})/) {

		($err_code, $result) = try_parse_time($1, "%Y%m%d%H%M%S", $verbose);
		$need_rewrite = 1;
		$date_valid = 0;

	} elsif ($timestring =~ /(\w{3},\s+\d{1,2}\s+\w{3}\s+\d{4}\s+\d{2}\.\d{2}\.\d{2}\s+[+-]\d{4})/) {

		($err_code, $result) = try_parse_time($1, "%a, %d %b %Y %H.%M.%S %z", $verbose);
		$need_rewrite = 1;
		$date_valid = 1;

	} elsif ($timestring =~ /(\d{1,2}\s+\w{3}\s+\d{4}\s+\d{2}([:\.])\d{2}([:\.])\d{2}\s+[+-]\d{4})/) {

		($err_code, $result) = try_parse_time($1, "%d %b %Y %H" . $2 . "%M" . $3 . "%S %z", $verbose);
		$need_rewrite = 1;
		$date_valid = 1;

	}

	
	if ($err_code) {
&log("Date match failed: $timestring.\n", 2);
		$result = gmtime('0');
		$need_rewrite = 1;
		$date_valid = 0;
	}

		# print "gm:          " . gmtime($result) . "\n";
		# print "converted to " . $result->strftime("%a, %d %b %Y %H:%M:%S +0000") . " (" . $result->epoch . ")\n";
	return ($date_valid, $need_rewrite, $result);
}

sub try_parse_time {
	my ($line, $format, $verbose) = @_;

	my $result;
	my $err_code = 0;

	eval { 
		$result = Time::Piece->strptime($line, $format);
&log("Converted $line to " . $result->strftime("%a, %d %b %Y %H:%M:%S" . " +0000") . "\n", 2);
		1;
	} or do {
&log("Failed to convert $line to datetime\n", -1);
		$err_code = 1;
		$result = gmtime(0);
	};

	return ($err_code, $result);
	
}
# Try to get a attachment, if necessary follow attachment pointer list recursively
# Params:
# - piler_id - ID of the mail
# - index    - Index of the attachment
# - dbhandle - Handle to the open database connection
#
# Returns:
# - result
#   0: Attachment was successfully downloaded
#   1: Attachment was not found in the attachment table (seems to related to 
#      issue #1194, where a fake attachment pointer is inserted into the mail)
#   2: Attachment points to another attachment (duplicate), but that is not in the list
# pilerget 400000005e21aff314d03d4c003d6ddc1043 | less
# pileraget 400000005e21aff314d03d4c003d6ddc1043 8 | less
# mysql piler -u pilerro --password=piler -e 'select * from v_attachment where piler_id="400000005e21aff314d03d4c003d6ddc1043";'
#+--------+--------------------------------------+---------------+--------+----------+
#| i      | piler_id                             | attachment_id | ptr    | refcount |
#+--------+--------------------------------------+---------------+--------+----------+
#| 549470 | 400000005e21aff314d03d4c003d6ddc1043 |             1 | 541479 |        0 |
#| 549471 | 400000005e21aff314d03d4c003d6ddc1043 |             2 | 169444 |        0 |
#| 549472 | 400000005e21aff314d03d4c003d6ddc1043 |             3 |  14209 |        0 |
#| 549473 | 400000005e21aff314d03d4c003d6ddc1043 |             4 |      0 |        0 |
#| 549474 | 400000005e21aff314d03d4c003d6ddc1043 |             5 | 541479 |        0 |
#| 549475 | 400000005e21aff314d03d4c003d6ddc1043 |             7 |      0 |        0 |
#| 549476 | 400000005e21aff314d03d4c003d6ddc1043 |             8 | 303647 |        0 |
#+--------+--------------------------------------+---------------+--------+----------+
# mysql piler -u pilerro --password=piler -e 'select * from v_attachment where i=303647;'               
#+--------+--------------------------------------+---------------+------+----------+
#| i      | piler_id                             | attachment_id | ptr  | refcount |
#+--------+--------------------------------------+---------------+------+----------+
#| 303647 | 40000000566ad7640df7ef740075abc95b76 |             3 |    0 |        3 |
#+--------+--------------------------------------+---------------+------+----------+
# pileraget 40000000566ad7640df7ef740075abc95b76 3 | base64 -d -i > test.pdf
sub retrieve_attachment {
	my $piler_id = shift @_;
	my $index = shift @_;
	my $dbhandle = shift @_;
	my $verbose = shift @_;

	my $orig_id = $piler_id;
	my $orig_index = $index;

	my $att = "";
	my $query;
	my $query_string;

	while ($att eq "") {

		# print "pileraget $piler_id $index\n";

		# First try to get the attachment regularly

		$att = `pileraget $piler_id $index`;

		if ($att ne "") {
&log("SUCCESS: pileraget $piler_id $index\n", 2);
			return (0, $att);
		} 

&log("RETRIEVAL FAILED: $piler_id attachment $index\n", 2);

		# That failed. Try to find a duplicate reference in the attachments table

		$query_string = "SELECT i,piler_id,attachment_id,ptr FROM v_attachment where piler_id='$piler_id' AND attachment_id=$index;";

&log("SQL: $query_string\n", 3);

		$query = $dbhandle->prepare($query_string);
		$query->execute();

		my $ptr = 0;
		my $rows = 0;

		while (my @row = $query->fetchrow_array()){

&log("$row[0]\t$row[1]\t$row[2]\t$row[3]\n", 3);
			$ptr = $row[3];
			$rows++;
  		}       

	  	$query->finish();

		if ($rows != 1) {
&log("No attachment found for $orig_id / $orig_index.\n", -1);
			return (1, "");
		}

		# Now we have a pointer to another record in the attachments table. Lets get it
		$query_string = "SELECT i,piler_id,attachment_id,ptr FROM v_attachment where i=$ptr;";

&log("SQL: query $query_string\n", 3);

        $query = $dbhandle->prepare($query_string);
        $query->execute();
		$rows = 0;

        if (my @row = $query->fetchrow_array()){
	
&log("$row[0]\t$row[1]\t$row[2]\t$row[3]\n", 3);
            $piler_id = $row[1];
			$index = $row[2];
			$rows += 1;
        }

        $query->finish();

		if ($rows != 1) {
&log( "No attachment found for $orig_id / $orig_index.\n", -1);
			return (2, "");
		}
	}

}

sub get_piler_id_for_id {
        my $id = shift @_;
        my $dbhandle = shift @_;


        my $query;
        my $query_string;

        $query_string = "SELECT id,piler_id FROM metadata where id='$id';";

        $query = $dbhandle->prepare($query_string);
        $query->execute();

        my $rows = 0;
        my $piler_id = 0;

        while (my @row = $query->fetchrow_array()){
                $piler_id=$row[1];
                $rows++;
        }

        $query->finish();

        if ($rows != 1) {
                return -1;
        }

        return $piler_id;
}

sub get_id_for_piler_id {
	my $piler_id = shift @_;
	my $dbhandle = shift @_;


        my $query;
        my $query_string;

        $query_string = "SELECT id,piler_id FROM metadata where piler_id='$piler_id';";

        $query = $dbhandle->prepare($query_string);
        $query->execute();

	my $rows = 0;
	my $id = 0;

        while (my @row = $query->fetchrow_array()){
		$id=$row[0];
                $rows++;
        }

        $query->finish();

        if ($rows != 1) {
                return -1;
        }

	return $id;
}
